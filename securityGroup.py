#!/usr/bin/env python3
import boto3

def ipList():
    ips = []
    for port in [80,443,22,3306]:
        if port == 80 or port == 443:
            ips.append(
            {'IpProtocol': 'tcp',
             'FromPort': port,
             'ToPort': port,
             'IpRanges': [{'CidrIp': '0.0.0.0/0'}]
            })
        else:
            ips.append(
            {'IpProtocol': 'tcp',
             'FromPort': port,
             'ToPort': port,
             'IpRanges': [{'CidrIp': '86.23.26.210/32'}]
            })
    return ips

def create(resource, name, description='Made using Python'):
    response = resource.create_security_group(Description = description, GroupName = name)
    groupID = response.id
    print("Security group created with ID: "+str(groupID))
    ips = ipList()

    data = response.authorize_ingress(GroupId=groupID, IpPermissions=ips)
    return groupID
