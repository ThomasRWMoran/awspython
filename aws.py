#!/usr/bin/env python3
import boto3
import securityGroup
import keyPair
import instance
import time
import requests
import sys
import prov

def giveMe5Sec(int=1):
    print("\nGive me "+str(int*5)+"secs")
    for i in range(0, int):
        print(".")
        time.sleep(5)
    print("\n")

#keyname = keyPair.create(ec2, 'thomasPython', 'pythonKey.pem')
#secGrpId = securityGroup.create(ec2, 'thomasPythonFirewall')

nginxVM = instance.createWprov('AcademySG', 'monitorKey', 'monitorNginx', prov.nginxProv())

giveMe5Sec(4)

apacheVM = instance.createWprov('AcademySG', 'monitorKey', 'monitorApache', prov.apacheProv())

giveMe5Sec(4)

lbVM = instance.createWprov('AcademySG', 'monitorKey', 'monitorLoadbalancer', prov.haproxyProv(instance.getPivIP(apacheVM[0]),instance.getPivIP(nginxVM[0])))
