#!/usr/bin/env python3
import boto3
import time

ec2 = boto3.resource('ec2',region_name='eu-west-2')

provfile = open('provision.txt','r')
provdata = provfile.read()
provfile.close()

myinstance = ec2.create_instances(
    ImageId ='ami-06ce3edf0cff21f07',
    InstanceType ='t2.micro',
    SecurityGroups = 'petClinicSecGrp',
    KeyName = 'jenkinsKey',
    MinCount = 1,
    MaxCount = 1,
    UserData = provdata
)

instIds = []
for instance in myinstance:
    print("Created instance with ID: "+instance.id)
    tag = ec2.create_tags(Resources=[instance.id], Tags=[
        {'Key':'Name', 'Value': 'TestPetClinic'},
        {'Key':'Owner', 'Value':'Tom Moran'},
        {'Key':'StartDate', 'Value':'20200507'},
        {'Key':'EndDate', 'Value':'20200507'}
    ])
    print("Booting up")
    instance.reload()
    while instance.state['Code'] == 0:
        print(".")
        time.sleep(5)
        instance.reload()

    print("\nAt Public IP: "+str(instance.public_ip_address))
