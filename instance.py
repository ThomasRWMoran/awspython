#!/usr/bin/env python3
import boto3
import securityGroup
import keyPair
import time
import urllib.request

def create(secGrp, keyname, tagname):
    ec2 = boto3.resource('ec2',region_name='eu-west-1')

    provfile = open('provision','r')
    provdata = provfile.read()
    provfile.close()

    myinstance = ec2.create_instances(
        ImageId ='ami-06ce3edf0cff21f07',
        InstanceType ='t3a.medium',
        SecurityGroups =[secGrp],
        KeyName = keyname,
        MinCount = 1,
        MaxCount = 1,
        UserData = provdata
    )

    instIds = []
    for instance in myinstance:
        print("Created instance with ID: "+instance.id)
        tag = ec2.create_tags(Resources=[instance.id], Tags=[
            {'Key':'Name', 'Value':tagname},
            {'Key':'Owner', 'Value':'Tom Moran'},
            {'Key':'StartDate', 'Value':'20200507'},
            {'Key':'EndDate', 'Value':'20200507'}
        ])
        print("Booting up", end ="")
        instance.reload()
        while instance.state['Code'] == 0:
            print(".", end ="")
            time.sleep(5)
            instance.reload()

        print("\nAt Public IP: "+str(instance.public_ip_address))
        instIds.append(instance.id)
    return instIds

def webAndImage(instanceID, imagename):
    ec2 = boto3.resource('ec2',region_name='eu-west-1')
    instance = ec2.Instance(instanceID)
    url = 'http://'+str(instance.public_ip_address)
    urlcode = urllib.request.urlopen(url).getcode()
    if urlcode == 200:
        image = instance.create_image(Name=imagename)
        print('Created image '+imagename+'. Id: '+str(image.image_id))
        return str(image.image_id)
    else:
        instance.terminate()

def createWprov(secGrp, keyname, tagname, prov):
    ec2 = boto3.resource('ec2',region_name='eu-west-1')

    myinstance = ec2.create_instances(
        ImageId ='ami-06ce3edf0cff21f07',
        InstanceType ='t3a.medium',
        SecurityGroups =[secGrp],
        KeyName = keyname,
        MinCount = 1,
        MaxCount = 1,
        UserData = prov
    )

    instIds = []
    for instance in myinstance:
        print("Created instance with ID: "+instance.id)
        tag = ec2.create_tags(Resources=[instance.id], Tags=[
            {'Key':'Name', 'Value':tagname},
            {'Key':'Owner', 'Value':'Tom Moran'},
            {'Key':'StartDate', 'Value':'20200625'},
            {'Key':'EndDate', 'Value':'20200626'}
        ])
        print("Booting up")
        instance.reload()
        while instance.state['Code'] == 0:
            print(".")
            time.sleep(5)
            instance.reload()

        print("\nAt Public IP: "+str(instance.public_ip_address))
        instIds.append(instance.id)
    return instIds

def getPivIP(instanceID):
    ec2 = boto3.resource('ec2',region_name='eu-west-1')
    instance = ec2.Instance(instanceID)
    return instance.private_ip_address
