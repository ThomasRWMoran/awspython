#!/usr/bin/env python3

def nginxProv():
    provfile = open('nginxProvision','r')
    provdata = provfile.read()
    provfile.close()
    return provdata

def apacheProv():
    provfile = open('apacheProvision','r')
    provdata = provfile.read()
    provfile.close()
    return provdata

def haproxyProv(apacheIP, nginxIP):
    provfile = open('haproxyProvision','r')
    provdata = provfile.read()
    ips = {'apachePrivIP': str(apacheIP), 'nginxPrivIP': str(nginxIP)}
    return provdata.format(**ips)
